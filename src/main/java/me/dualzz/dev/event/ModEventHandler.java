package me.dualzz.dev.event;

import me.dualzz.dev.KeyStrokes;
import me.dualzz.dev.key.container.KeyContainer;
import me.dualzz.dev.key.record.KeyRecorder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

/**
 * Handles all events for the mod
 */

public class ModEventHandler {

    @SubscribeEvent
    public void onRenderTick(TickEvent.RenderTickEvent event){
        if (Minecraft.getMinecraft().inGameHasFocus) {
            KeyStrokes keyStrokes = KeyStrokes.getInstance();
            KeyContainer container = keyStrokes.getKeyContainer();

            ScaledResolution scaledresolution = new ScaledResolution(Minecraft.getMinecraft());
            int width = scaledresolution.getScaledWidth();

            // Render in the top right corner
            container.render(width - container.getWidth() - container.getSpacing(), container.getSpacing());
        }
    }

    @SubscribeEvent
    public void onMouseInput(InputEvent.MouseInputEvent event){
        if (Mouse.getEventButtonState()) {
            // This is how Minecraft represents mouse clicks as a key code
            processKeyInput(Mouse.getEventButton() - 100);
        }
    }

    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent event){
        if (Keyboard.getEventKeyState()) {
            processKeyInput(Keyboard.getEventKey());
        }
    }

    private void processKeyInput(int keyCode){
        KeyRecorder keyRecorder = KeyStrokes.getInstance().getKeyRecorder();

        if (keyRecorder.isKeyRecorded(keyCode)) {
            keyRecorder.onAction(keyCode);
        }
    }
}
