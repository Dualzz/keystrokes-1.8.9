package me.dualzz.dev;

import me.dualzz.dev.event.ModEventHandler;
import me.dualzz.dev.key.Key;
import me.dualzz.dev.key.EmptyKey;
import me.dualzz.dev.key.container.KeyContainer;
import me.dualzz.dev.key.container.KeyContainerBuilder;
import me.dualzz.dev.key.container.KeyRow;
import me.dualzz.dev.key.record.KeyRecorder;
import me.dualzz.dev.key.record.RecordedKey;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

/**
 * Main class of the mod
 */

@Mod(modid = "keystrokes", name = "KeyStrokes", version = "1.0-SNAPSHOT")
public class KeyStrokes {

    private static KeyStrokes instance;
    private KeyRecorder keyRecorder;
    private KeyContainer keyContainer;

    @Mod.EventHandler
    public void onInit(FMLInitializationEvent event){
        instance = this;
        keyRecorder = new KeyRecorder();

        GameSettings settings = Minecraft.getMinecraft().gameSettings;

        // Empty key used for spacing
        Key empty = new EmptyKey();

        Key forward = new Key(settings.keyBindForward);
        Key left = new Key(settings.keyBindLeft);
        Key back = new Key(settings.keyBindBack);
        Key right = new Key(settings.keyBindRight);

        // We want to record and display the clicks per second of these keys
        Key attack = new RecordedKey(settings.keyBindAttack);
        Key useItem = new RecordedKey(settings.keyBindUseItem);

        Key jump = new Key(settings.keyBindJump);

        keyContainer = new KeyContainerBuilder()
                // Set resolutions
                .setWidth(77)
                .setHeight(100)
                .setSpacing(1)
                .setRowSpacing(1)
                .setKeySpacing(1)
                // Add rows with specified key sizes
                .addRow(new KeyRow(25, 25))
                .addRow(new KeyRow(25, 25))
                .addRow(new KeyRow(38, 25))
                .addRow(new KeyRow(77, 25))
                // Add keys
                .addKeys(0, empty, forward)
                .addKeys(1, left, back, right)
                .addKeys(2, attack, useItem)
                .addKeys(3, jump)
                .build();

        // Register event handler
        FMLCommonHandler.instance().bus().register(new ModEventHandler());
    }

    public static KeyStrokes getInstance() {
        return instance;
    }

    public KeyContainer getKeyContainer() {
        return keyContainer;
    }

    public KeyRecorder getKeyRecorder() {
        return keyRecorder;
    }
}
