package me.dualzz.dev.key.container;

import me.dualzz.dev.key.Key;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a container holding {@link Key}s which are stored in {@link KeyRow}s.
 * Has specified values which the keys use to determine their position on the screen.
 */

public class KeyContainer {

    protected final List<KeyRow> rows = new ArrayList<>();

    protected int width;
    protected int height;
    protected int spacing;

    protected int rowSpacing;
    protected int keySpacing;

    // Only allow creation with builder and extending classes
    protected KeyContainer(){
    }

    public void render(int x, int y){
        // Create new matrix and apply the containers screen position
        GL11.glPushMatrix();
        GL11.glTranslated(x, y, 0.0);

        // Render each row
        for (KeyRow row : rows) {
            // Copy matrix on the stack for each key to apply per-key translations
            GL11.glPushMatrix();

            //Render each key
            for (Key key : row.getKeys()) {
                key.render(row.getKeyWidth(), row.getKeyHeight());
                double offset = row.getKeyWidth() + keySpacing;
                // Translate for every key
                GL11.glTranslated(offset, 0.0, 0.0);
            }

            // Pop matrix for keys off the stack and translate row matrix
            GL11.glPopMatrix();
            GL11.glTranslated(0.0, row.getKeyHeight() + rowSpacing, 0.0);
        }

        // Clean up
        GL11.glPopMatrix();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getSpacing() {
        return spacing;
    }
}
