package me.dualzz.dev.key.container;

import com.google.common.base.Preconditions;
import me.dualzz.dev.key.Key;

/**
 * This class is used to build a {@link KeyContainer}
 * Data can be specified using the public methods
 */

public class KeyContainerBuilder {

    private KeyContainer container;

    public KeyContainerBuilder(){
        container = new KeyContainer();
    }

    // If you want to build from an existing container
    public KeyContainerBuilder(KeyContainer container){
        this.container = container;
    }

    public KeyContainerBuilder setWidth(int width){
        Preconditions.checkArgument(width > 0);
        container.width = width;
        return this;
    }

    public KeyContainerBuilder setHeight(int height){
        Preconditions.checkArgument(height > 0);
        container.height = height;
        return this;
    }

    public KeyContainerBuilder setSpacing(int spacing){
        Preconditions.checkArgument(spacing > 0);
        container.spacing = spacing;
        return this;
    }

    public KeyContainerBuilder setRowSpacing(int spacing){
        Preconditions.checkArgument(spacing > 0);
        container.rowSpacing = spacing;
        return this;
    }

    public KeyContainerBuilder setKeySpacing(int spacing){
        Preconditions.checkArgument(spacing > 0);
        container.keySpacing = spacing;
        return this;
    }

    public KeyContainerBuilder addKeys(int row, Key... keys){
        Preconditions.checkArgument(container.rows.get(row) != null);
        for (Key key : keys) {
            container.rows.get(row).getKeys().add(key);
        }
        return this;
    }

    public KeyContainerBuilder addRow(KeyRow row){
        container.rows.add(row);
        return this;
    }

    public KeyContainer build(){
        return container;
    }
}
