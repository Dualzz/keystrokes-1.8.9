package me.dualzz.dev.key.container;

import me.dualzz.dev.key.Key;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a row holding {@link Key}s
 * Determines the height and width of the stored keys
 */

public class KeyRow {

    private final List<Key> keys = new ArrayList<>();

    private int keyWidth;
    private int keyHeight;

    public KeyRow(int keyWidth, int keyHeight){
        this.keyWidth = keyWidth;
        this.keyHeight = keyHeight;
    }

    public int getKeyHeight() {
        return keyHeight;
    }

    public int getKeyWidth() {
        return keyWidth;
    }

    public List<Key> getKeys() {
        return keys;
    }
}
