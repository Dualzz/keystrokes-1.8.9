package me.dualzz.dev.key;

import me.dualzz.dev.key.container.KeyRow;

/**
 * Simply used as a spacer key since the positions in the {@link KeyRow}s are sorted from the left
 */

public class EmptyKey extends Key {

    public EmptyKey(){
        super(null);
    }

    // Of course, don't render anything for this key
    @Override
    public void render(int width, int height){
    }

    @Override
    public String getText(){
        return null;
    }
}