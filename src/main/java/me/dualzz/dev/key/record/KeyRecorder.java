package me.dualzz.dev.key.record;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.util.*;

/**
 * Records the time of input of key presses and mouse clicks
 * Can calculate the actions per second of a key
 */

public class KeyRecorder {

    private final Set<Integer> recordedKeys = new HashSet<>();
    private final Multimap<Integer, Long> actionMap = HashMultimap.create();

    public void onAction(int keyCode) {
        actionMap.put(keyCode, System.currentTimeMillis());
    }

    public int getActionsPerSecond(int keyCode) {
        // Remove all entries that are longer than a second ago
        removeExpired(1000);
        return actionMap.containsKey(keyCode) ? actionMap.get(keyCode).size() : 0;
    }

    private void removeExpired(long delay){
        long currentTime = System.currentTimeMillis();
        for (int keyCode : recordedKeys){
            if (actionMap.containsKey(keyCode)) {
                Collection<Long> timings = actionMap.get(keyCode);
                timings.removeIf(timing -> currentTime - timing - delay > 0);
            }
        }
    }

    public boolean isKeyRecorded(int keyCode){
        return recordedKeys.contains(keyCode);
    }

    public void recordKey(int keyCode){
        recordedKeys.add(keyCode);
    }

    public void unRecordKey(int keyCode){
        recordedKeys.remove(keyCode);
        actionMap.removeAll(keyCode);
    }
}
