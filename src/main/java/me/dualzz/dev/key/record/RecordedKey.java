package me.dualzz.dev.key.record;

import me.dualzz.dev.KeyStrokes;
import me.dualzz.dev.key.Key;
import net.minecraft.client.settings.KeyBinding;

/**
 * {@link Key} that displays the activations per second below the key name
 */

public class RecordedKey extends Key {

    private int lastKeyCode;

    public RecordedKey(KeyBinding keyBinding){
        super(keyBinding);
    }

    @Override
    public void render(int width, int height){
        int keyCode = keyBinding.getKeyCode();

        if (lastKeyCode != keyCode){
            KeyRecorder manager = KeyStrokes.getInstance().getKeyRecorder();
            manager.unRecordKey(lastKeyCode);
            manager.recordKey(keyCode);
        }

        lastKeyCode = keyCode;

        super.render(width, height);
    }

    @Override
    public void renderText(int width, int height, String text){
        int keyCode = keyBinding.getKeyCode();
        int cps = KeyStrokes.getInstance().getKeyRecorder().getActionsPerSecond(keyCode);
        int offset = height / 4;

        super.renderText(width, height/2 + offset, text);
        super.renderText(width, height + height/2 + offset, "CPS: " + cps, 0.8f);
    }
}
