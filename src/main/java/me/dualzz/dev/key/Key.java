package me.dualzz.dev.key;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import org.apache.commons.lang3.StringEscapeUtils;
import org.lwjgl.opengl.GL11;

/**
 * Represents a key on the keyboard or a mouse button.
 */

public class Key {

    protected final KeyBinding keyBinding;

    public Key(KeyBinding keyBinding) {
        this.keyBinding = keyBinding;
    }

    // Renders the complete key
    public void render(int width, int height){
        renderQuad(width, height);
        renderText(width, height, getText());
    }

    // Renders the quad for the key
    protected void renderQuad(int width, int height){
        // If the key is pressed show a brighter shade
        boolean pressed = GameSettings.isKeyDown(keyBinding);
        float brightness = pressed ? 0.8f : 0.3f;

        // Set color in openGL
        GL11.glColor4f(brightness, brightness, brightness, 0.7f);

        // Set blending and disable texture
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);

        // Make a simple quad
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glVertex3d(0.0, height, 0);
        GL11.glVertex3d(width, height, 0);
        GL11.glVertex3d(width, 0.0, 0);
        GL11.glVertex3d(0.0, 0.0, 0);
        GL11.glEnd();

        // Reset OpenGL constants
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    protected void renderText(int width, int height, String text){
        this.renderText(width, height, text, 1.0f);
    }

    // Renders the text for the key (method overloading for optional scale)
    protected void renderText(int width, int height, String text, float scale){
        // Reset colour to white for the text
        GL11.glColor4f(1,1,1, 1);

        // Get font renderer and render text in middle (1 extra pixel because it looks better)
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRendererObj;
        int textWidth = fontRenderer.getStringWidth(text);
        int x = (int) (width - textWidth * scale) / 2 + 1;
        int y = (int) (height - fontRenderer.FONT_HEIGHT * scale) / 2 + 1;

        // Apply scaling and render font
        GL11.glPushMatrix();
        GL11.glScalef(scale, scale, 1);
        fontRenderer.drawString(text, x, y, 0xFFFFFF);
        GL11.glPopMatrix();
    }

    protected String getText() {
        int keyCode = keyBinding.getKeyCode();

        // Some renames because some key names are too long or don't look good
        switch (keyCode) {
            case -100:
                return "LMB";
            case -99:
                return "RMB";
            case -98:
                return "MMB";
            case 29:
                return "LCTRL";
            case 157:
                return "RCTRL";
            case 56:
                return "LALT";
            case 184:
                return "RALT";
            case 57:
                // Space key is set to display a line
                ChatComponentText component = new ChatComponentText("-------");
                component.getChatStyle().setStrikethrough(true);
                return component.getFormattedText();
            default:
                return GameSettings.getKeyDisplayString(keyCode);
        }
    }
}
